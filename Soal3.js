// soal no 3 
function checkEmail(email=-1) {
    let at =/[@]/g
    let domain = /.com|.co.id/g
    if(email==-1)
      return "EROR"
  if(email.search(at)!=-1){
    if(email.search(domain)!=-1){
      return "VALID"
    }else{
      return "INVALID"
    }
  }
    return "EROR"
  }
  
  console.log(checkEmail('apranata@binar.co.id'))
  console.log(checkEmail('apranata@binar.com'))
  console.log(checkEmail('apranata@binar'))
  console.log(checkEmail('apranata')) // string bernilai eror karena pada syntax kode tidak ada @.com atau @.co.id
  console.log(checkEmail('3322')) // string bernilai eror karena pada syntax kode tidak ada @.com atau @.co.id dan juga angka tidak termasuk pola pada syntax ini
  console.log(checkEmail()) // string bernilai eror karena nilai pada parameter kosong
  